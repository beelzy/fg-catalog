# fg-catalog

![screenshot](https://gitlab.com/beelzy/fg-catalog/raw/master/screenshot.png)

Click [here](https://beelzy.gitlab.io/fg-catalog) for a web demo version of this. The web demo doesn't have any settings since most of them are NW.js specific.

fg-catalog is a flash game catalog and launcher that uses Pepper Flash with NW.js under the hood to leverage proper GPU acceleration for Linux users. This allows you to play games made with Citrus Engine or Starling without too many performance issues.

In order for this to work, the version of the Chromium driver used by NW.js must stay consistent and cannot increase. Specific measures have been taken to prevent this as much as possible.

What fg-catalog is for:
* Provide a catalog of flash games in a modern browser environment while being minimal on resources as much as possible
* Provide a Linux friendly way to run flash games with full support and proper GPU acceleration
* Run games offline and block unwanted ads
* Continue to be able to play flash games after 2020
* Run flash games in their native resolutions as they're intended to run in, or provide ways to run pure vector games in larger resolutions
* Be able to run games from any location accessible on disk in any folder structure as desired by the user

fg-catalog does not provide any games; just the framework required to browse and play the ones you already have.

## Developing

* `git clone` this project, and then `cd` into the project root and run `git submodule init` and `git submodule update` to get the metadata.
* Run `npm install` to set up build environment.
* Download [NW.js](https://nwjs.io/downloads/) (currently v0.36.2). For development, download the SDK version. For releases, download the normal version.
* Unzip/untar NW.js in the root directory of the project and rename the NW.js folder `dist` or `build`, depending on which one you plan to use.
* Run `npm run build` for development builds and `npm run release` for releases.

* Run the app by executing `./dist/nw` or `./build/nw`. You may also use the `fg-catalog` script on Linux to automatically execute with the `no-internet` script.

## Flash Version Warning Feature

This app has a special feature where it will warn you if the flash version you are using is too high and contains the kill switch which causes flash player to stop functioning on or after January 12th, 2021. Please note we do not provide older versions of the flash player, and Adobe no longer provides those. Since the target audience of this app is predominantly Linux users, most distros that have a stable or LTS repo will probably have an earlier version of the flash player that does not have the kill switch anyways.
NOTE: It seems most distros have removed the flash plugins/players from their repos because of the EOL. You can still obtain a copy from [archive.org](https://archive.org/details/flashplayer_old).

## Chromium Version Warning Feature

This app has a special feature where you can check if the version of the Chromium driver has changed since it's been included. The static, expected version of Chromium driver is specified in `package.json` under the property `chrome-version`. If the current version of the Chromium version running in the app does not match this static version, a notification will appear in the icons in the toolbar and an overlay can be displayed to see the actual and expected versions of the Chromium driver. You can, if you prefer, use earlier versions of NW.js to cut down on file size, but versions of NW.js < 0.36.2 have not been tested.

## Flash Security Notes

### Flash Control Buttons

* Makes use of the object native calls, `Play()` and `Rewind()` accessible in the DOM <object> tag.
* In order for this to work, the app location itself must be added to the global flash security allowed locations.

### Flash Requests

* A base path is included to ensure the other flash resources are loaded correctly.
* The base path is usually under something like `file:///`, and resides somewhere separately from the rest of the app (`chrome-extension://`). This means the base path also needs to be whitelisted in flash security allowed locations.
* Http requests that aren't from trusted LAN locations are blocked.

## PPAPI Flash Path

* This app does not bundle `pepper-flash`. You are encouraged to obtain it yourself through your package manager if it's still there or from archive.org ([see above](#flash-version-warning-feature)). You can tell the app where this plugin resides by going into the settings overlay and adding the correct path pointing to `libpepflashplugin.so`.
* A sensible (for Arch Linux) PPAPI path is provided by default. This can be changed in the settings overlay, but the save and restart button currently can't cause the Chromium driver to reload the flash plugin. For now, you'll have to manually restart the app by quitting and reopening it.
* Using the debug flash plugin or certain versions of it may result in very long url request timeouts, which can block loading or running the rest of the game. Switch back to the regular one to have the games resume normally.

## Sandboxing

* This app is designed with external http request sandboxing in mind. This is done to block requests from:
** Ads from flash games
** Google phoning home
** Tracking, either from the flash games or Google

External http requests are blocked internally by the app, but will be overridden by certain Google calls. To further prevent this from happening, it is recommended to run this app with an external `no-internet` script that completely blocks all external requests except for LAN calls, and runs outside of the app. This script is actually just some iptable rules on Linux.

### `no-internet` Script

A script that makes use of a `no-internet` group and some iptable rules on Linux to block external Internet connections. There may be alternative ways of doing this on other platforms, but instructions for how this works exactly can be found [here](https://serverfault.com/questions/550276/how-to-block-internet-access-to-certain-programs-on-linux) and [here](https://www.privateinternetaccess.com/archive/forum/discussion/28770/my-iptables-rules-to-prevent-leaks-and-allow-lan-access).


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
