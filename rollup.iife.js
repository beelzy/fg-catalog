/* globals process */
/*
Build to a module that has ES2015 module syntax but otherwise only syntax features that node supports
https://github.com/rollup/rollup/wiki/jsnext:main
*/
import { pkg, createConfig } from "./rollup.base.js";

const includeDependencies = true; // Use `false` if you are creating a library, or if you are including external script in html
const env = process.env; // eslint-disable-line no-undef

const format = 'iife';

const baseConfig = createConfig({
    includeDependencies,
    input: "src/index.js",
    output: "public/app.js",
    sass: 'style.css',
    format: format
});

const targetConfig = function(config) {
    return Object.assign({}, config, {
        output: Object.assign(
            {},
            config.output,
            {
                file: config.output.name,
                format: format
            }
        )
    });
};

export default targetConfig(baseConfig);
