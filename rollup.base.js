import fs from "fs";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import pathmodify from "rollup-plugin-pathmodify";
import scss from "rollup-plugin-scss";
import del from "rollup-plugin-delete";
import ignore from "rollup-plugin-ignore";
import { builtinModules } from "module";
import terser from "@rollup/plugin-terser";

export const pkg = JSON.parse(fs.readFileSync("./package.json"));
if (!pkg) {
  throw("Could not read package.json");
}
const env = process.env; // eslint-disable-line no-undef
const input = env.INPUT || "src/index.js";
const name = env.NAME || "dist/package.nw/app.js";
const external = Object.keys(pkg.dependencies || {});

const globals = {};
const isDev = process.argv.length >= 5 && process.argv.indexOf('--config-dev') > -1;

external.forEach(ext => {
  switch (ext) {
  case "mithril":
    globals["mithril"] = "m";
    break;
  default:
    globals[ext] = ext;
  }
});

let plugins = function(output, sass, format) {
    let pluginsList = [
        del({targets: output + '*'}),
        // Resolve libs in node_modules
        resolve({
            jsnext: true,
            main: true,
            browser: true
        }),

        pathmodify({
            aliases: [
                {
                    id: "mithril/stream",
                    resolveTo: "node_modules/mithril/stream.js"
                }
            ]
        }),

        // Convert CommonJS modules to ES6, so they can be included in a Rollup bundle
        // Ignore native node modules and game data
        commonjs(format === 'es' ? {ignore:  function(id) {
            return id.startsWith('./data/') || ['fs', 'path', 'os', './package.json'].indexOf(id) >= 0;
        }} : {})
    ];

    if (format === 'iife') {
        pluginsList.push(ignore([...builtinModules,
            './package.json',
            './data/config.json',
            './data/catalog.json',
            './data/dimensions.json',
            './data/creators.json'
        ]));
    }

    if (sass) {
        pluginsList.push(scss({fileName: sass, sourceMapEmbed: isDev}));
    }

    if (!isDev) {
        pluginsList.push(terser());
    }
    return pluginsList;
};

export const createConfig = ({ includeDependencies, input, output, sass, format }) => {
    return {
        input,
        external: includeDependencies ? [] : external,
        output: {
            name: output,
            globals,
            sourcemap: isDev ? 'inline' : false
        },
        plugins: plugins(output, sass, format)
    };
};
