#!/bin/sh

cp -r static/fonts/ public/;
cp -r static/icons/ public/;
cp package.json public/;
