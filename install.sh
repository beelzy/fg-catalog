#!/bin/sh

rm -rf $1/package.nw/;
mkdir $1/package.nw/;
cp fg-catalog $1/;
cp -r static/* $1/package.nw/;
cp package.json $1/package.nw/;
cp -r data $1/package.nw/;
