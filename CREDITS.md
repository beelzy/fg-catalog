# SVG Icons

- [Html5_3d_effects.svg](https://www.w3.org/html/logo/index.html#downloads) by W3C, CC-BY 3.0
- [puff.svg](https://github.com/SamHerbert/SVG-Loaders) by SamHerbert, MIT
- [stop.svg](https://www.svgrepo.com/svg/125641/stop) by Freepik, CC-BY 4.0
- All other icons by Font Awesome, CC-BY 4.0
