# Changelog

## v2.3.2

* Use public folder for web assets.
* Use correct creator's name in filter in the toolbar.
* Warn when flash version contains kill switch.

## v2.3.1

* No longer need to pass routeChanged variable around. Tabs tracks this on its own.
* Add web version for Gitlab demo.
* Minor enhancements.
* Minify web assets.

## v2.3.0

* Cleanup build process.
* Include metadata as a git submodule.
* Update README.
* Include convenience script that uses `no-internet`.
* Make LAN domain whitelisting more agnostic.
* Cleanup and attribute svg icons.
* Better error handling when metadata is missing.
* Clean up flash trust settings on startup.
* Add license.

## v2.2.1

* Removed the global settings option. Until the Shared Object gets saved properly, we can't make use of it.
* Open creator and notes links externally.
* Display creator's name in the game toolbar properly.
* Calculate window height correctly by considering launch in window option.
* Improve visibility for title and creator in the toolbar when games have smaller widths.
* Use release build of NW.js (can still keep the sdk one for development).

## v2.2.0

* Can now launch games in a new window. Use the settings option for this.
* Expanded rollup configs.

## v2.1.4

* Put default icons in an SVG manifest variable.
* Add rollup plugin for cleaning up build files.
* Don't include sourcemaps (js and css) on release builds.
* Modularize style components.

## v2.1.3

* Correctly obtain user's home folder for flash-trust `customFolder`.
* Add and update README.
* Open external links in default browser.
* More streamlined animations when navigating between catalog and game states.
* Support flash game dimensions as aspect ratios for vector based games.
* Remove unneeded icons.
* Use sensible defaults for PPAPI flash path.
* Refactoring.
* Replace XMLHttpRequests with `require`.

## v2.1.2

* Restrict window size to dimensions smaller than the screen size.
* Fullscreen embed content is centered properly.
* Don't move the window during fullscreen. This causes the app to freeze up.
* Don't block http urls or known domains on the LAN.
* Make flash controls work again by properly adding the app to flash security allowed locations.
* Calculate flash embed dimensions more systematically.

## v2.1.1

* package.json becomes the source of truth for version number.
* Update gitignore.
* Block unwanted navigation and requests.
* Disable Google phone home.
* Track game data files in a separate repo.

## v2.1.0

* Fix snap to item scrolling when app loses or gains focus.
* Update catalog data.
* Create tag-button styles.
* Add a play button.
* Add ability to run games in the browser.
* Properly manage flash trust of the swf path in the settings.
* Update game data.

## v2.0.4

* Add extra build option to build with or without terser. `npm run build` or `npm run release`.
* Loading json files only results in a single m.redraw call rather than 3.
* Clean up code and move global view variables into models.
* Add save and restart functionality to settings.

## v2.0.3

* Give toolbar and tabs their own views.
* Add missing Chromium icon.
* More refactoring. Fewer variables unnecessarily passed around.
* Restructure SCSS.
* Add settings menu.

## v2.0.2

* Increase performance by passing variables directly into components instead of the entire vnode.attrs object. This reduces the number of unnecessary redraws.
* Refactoring vnode.children.
* Add Chromium driver version detection.

## v2.0.1

* Move event handlers in list view into its own helper class.
* Add window size and position functions.
* Update gitignore.

## v2.0.0

* Switch to es format.
* Bundle with NW.js.
* Change folder structure. Dist folder is now package.nw inside dist, and dist contains the NW.js files.

## v1.1.2

* Fix infinite scrolling placeholder widths on window resize.
* Reset isMouseScroll when mouse leaves the app.

## v1.1.1

* Display loading indicator when infinite scrolling hasn't been created yet.

## v1.1.0

* Now uses `mithril-infinite` to increase performance when displaying and filtering bigger catalogs.
* Loading indicator appears when trying to jump to a page that doesn't yet have a placeholder.
* Titles that are too long are truncated with CSS ellipsis. The full title can be read by using the title tooltip hover.
* Mithril bundler is only a temporary solution, and will not be maintained in the future. So we have replaced it with Rollup.
* Improve snap to item functionality so that it doesn't conflict with inertia scrolling.
* Add new icons for the PPAPI Wrapper and Stage3D features.
* Synchronize creator data before displaying the list.

## v1.0.9

* Creators tab is sorted. 

## v1.0.8

* Add Flash logo.

## v1.0.6

* Update display of frame filter information on route change.
* Update info box.

## v1.0.4

* Update snap to item while scrolling when window size changes.
* Various styling and bug fixes.
