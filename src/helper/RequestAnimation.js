var RequestAnimation = {
    requestTimeout: function(fn, delay) {
        var start = new Date().getTime();
        var handle = new Object();
        var loop = function() {
            var current = new Date().getTime();
            var delta = current - start;
            delta >= delay ? fn.call() : handle.value = window.requestAnimationFrame(loop);
        };

        handle.value = window.requestAnimationFrame(loop);
        return handle;
    }
};

module.exports = RequestAnimation;
