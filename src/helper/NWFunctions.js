var Config = require('../models/Config');

module.exports = {
    win: window.webMode ? window : window.nw.Window.get(),
    DEFAULT_WIDTH: 1280,
    DEFAULT_HEIGHT: 720,
    initWindowPolicy: function() {
        module.exports.win.on('new-win-policy', function (frame, url, policy) {
            policy.setNewWindowManifest({resizable: false});
        });
        // Block navigation from flash apps as best as possible.
        module.exports.win.on('navigation', function(frame, url, policy) {
            if(url.startsWith('http')) {
                policy.ignore();
            }
        });
        window.onbeforeunload = function(e) {
            e.preventDefault();
            e.returnValue = '';
            return;
        };
        module.exports.resizeWindow(
            Config.configs.hasOwnProperty('windowsize')
            ? Config.configs.windowsize
            : undefined);
    },
    initChromePolicy: function() {
        // Block outside requests
        chrome.webRequest.onBeforeRequest.addListener(
            function(details) {
                var whitelist = Config.configs.allowDomains;
                var size = whitelist.length;
                var isWhitelistedDomain = false;
                for (var i = 0; i < size; i++) {
                    if (details.url.startsWith(whitelist[i])) {
                        isWhitelistedDomain = true;
                        break;
                    }
                }
                if (details.url.startsWith('http://192.168.0.') || isWhitelistedDomain) {
                    return;
                }
                return {cancel: true};
            },
            {urls:['http://*/*', 'https://*/*']},
            ['blocking']
        );
    },
    centerWindow: function(width, height) {
        var x = window.screen.availLeft + Math.round((window.screen.availWidth - width) / 2);
        var y = window.screen.availTop + Math.round((window.screen.availHeight - height) / 2);
        module.exports.win = nw.Window.get();
        if (x !== window.screen.availLeft || y !== window.screen.availTop) {
            module.exports.win.moveTo(x, y);
        }
        if (width === module.exports.resizeDimensions[0] && height === module.exports.resizeDimensions[1]) {
            module.exports.win.removeListener('resize', module.exports.centerWindow);
        }
    },
    resizeDimensions: [],
    resizeWindow: function(dimensions = {
        width: module.exports.DEFAULT_WIDTH,
        height: module.exports.DEFAULT_HEIGHT
    }) {
        module.exports.win.setResizable(true);
        module.exports.win.on('resize', module.exports.centerWindow);
        module.exports.resizeDimensions = [dimensions.width, dimensions.height];
        module.exports.win.resizeTo(dimensions.width, dimensions.height);
        module.exports.win.setResizable(false);
    }
}
