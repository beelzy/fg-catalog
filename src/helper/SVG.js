var m = require('mithril');

var manifest = {
    tag: {
        src: 'fa-tag.svg',
        size: [1536, 1792]
    },
    user: {
        src: 'fa-user-circle.svg'
    },
    times: {
        src: 'fa-times.svg',
        size: [1408, 1792],
        cssClass: '.close'
    },
    'sort-asc': {
        src: 'fa-sort-amount-asc.svg',
        cssClass: '.sort'
    },
    'sort-desc': {
        src: 'fa-sort-amount-desc.svg',
        cssClass: '.sort'
    },
    reset: {
        src: 'redo-alt-solid.svg',
        size: [512, 512]
    },
    play: {
        src: 'fa-play.svg',
        size: [1408, 1792]
    },
    'play-o': {
        src: 'fa-play-circle-o.svg',
        size: [1536, 1792]
    },
    link: {
        src: 'fa-link.svg',
        size: [1664, 1792]
    },
    'sign-in': {
        src: 'fa-sign-in.svg',
        size: [1536, 1792]
    },
    flash: {
        src: 'flash.svg',
        size: [120, 100],
        position: [20, 16]
    },
    list: {
        src: 'fa-list-alt.svg'
    },
    tags: {
        src: 'fa-tags.svg'
    },
    question: {
        src: 'fa-question-circle.svg',
        size: [1536, 1792]
    },
    settings: {
        src: 'fa-cog.svg',
        size: [1536, 1792]
    },
    chrome: {
        src: 'chrome.svg',
        size: [1000, 1000]
    },
    globe: {
        src: 'fa-globe.svg',
        size: [1536, 1792]
    },
    save: {
        src: 'fa-save.svg',
        size: [1536, 1792]
    },
    reload: {
        src: 'sync-alt-solid.svg',
        size: [512, 512]
    }
};

module.exports = {
    icon: function(key) {
        var icon = manifest[key];
        return module.exports.template(icon.src, key, icon.size, icon.cssClass, icon.title, icon.position, icon.size2);
    },
    template: function(src, id, size, cssClass, title, position, size2) {
        size = size ? size : [1792, 1792];
        size2 = size2 ? size2 : size;
        position = position ? position : [0, 0];
        return m('svg' + (cssClass ? cssClass : '')
            + '[viewBox="' + position[0] + ' ' + position[1] + ' ' + size[0] + ' ' + size[1] + '"]',
            [
                title ? m('title', title) : null,
                m('use[xlink:href=icons/'
                    + src + '#' + id + '][height="'
                    + size2[1] + 'px"][width="'
                    + size2[0] + 'px"]'
                )
            ]
        );
    }
};
