var m = require('mithril');
var infinite = require('mithril-infinite');

/*
 * Options
 *
 * count - {int} Total number of items in the scroll container.
 * itemsPerPage - {int} Number of items in every page.
 * itemRender - {function} Function that gets called when rendering items. Should return an element.
 * dataFn - {function} Function that returns data objects for every item that gets rendered. Needed for pageData.
 */

module.exports = {
    createOptions: function(options) {
        return {
            maxPages: Math.ceil(options.count / options.itemsPerPage),
            item: options.itemRender,
            pageData: function(index) {
                var pageItems = [];
                index--;
                var j = index * options.itemsPerPage;
                var size = Math.min(j + options.itemsPerPage, options.count);
                for (; j < size; j++) {
                    pageItems.push(options.dataFn(j));
                }
                return pageItems;
            }
        };
    }
};
