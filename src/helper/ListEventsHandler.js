var List = require('../models/List');
var RequestAnimation = require('./RequestAnimation');

/**
 * Events handling for list view.
 */

module.exports = {
    viewVnode: undefined,
    vnode: undefined,
    isScrolling: false,
    isMouseScroll: false,
    scrollCallback: undefined,
    mouseupCallback: undefined,
    scrollPos: undefined,
    scrollTime: undefined,
    pages: undefined,
    loadingIcon: undefined,
    //Snap while scrolling functionality
    snapToItem: function() {
        var scrollLeft = module.exports.vnode.dom.scrollLeft;
        // Don't snap on inertia scroll
        if (scrollLeft === module.exports.scrollPos) {
            var viewportWidth = module.exports.vnode.dom.offsetWidth;
            var delta = scrollLeft % viewportWidth;
            if (delta > viewportWidth / 2) {
                delta = delta - viewportWidth;
            }
            module.exports.vnode.dom.scrollLeft = scrollLeft - delta;
        }
    },
    mouseGone: function() {
        if (module.exports.isScrolling) {
            module.exports.snapToItem();
            // We don't actually want scroll events emitted by snapToItem to be caught by scroll listener as not mouse events, so we set a timer for that.
            module.exports.mouseupCallback = RequestAnimation.requestTimeout(function() {
                module.exports.isMouseScroll = false;
                module.exports.isScrolling = false;
            }, 100);
        }
        module.exports.isMouseScroll = false;
        module.exports.isScrolling = false;
    },
    scrollHandler: function(e) {
        // Calculate appropriate timeout, based on scroll speed
        var newScrollPos = module.exports.vnode.dom.scrollLeft;
        var delta = Math.abs(module.exports.scrollPos - newScrollPos);
        module.exports.scrollPos = newScrollPos;
        var newScrollTime = new Date().getTime();
        var deltaTime = newScrollTime - module.exports.scrollTime;
        module.exports.scrollTime = newScrollTime;
        var timeout = module.exports.scrollTime ? Math.max(100, Math.min(500, 100 * deltaTime / delta)) : 100;
        if (module.exports.scrollCallback) {
            window.cancelAnimationFrame(module.exports.scrollCallback.value);
        }
        if (!module.exports.isMouseScroll) {
            module.exports.scrollCallback = RequestAnimation.requestTimeout(function() {
                window.cancelAnimationFrame(module.exports.scrollCallback.value);
                module.exports.snapToItem();
                module.exports.isScrolling = false;
                module.exports.scrollTime = undefined;
            }, timeout);
        } else {
            module.exports.isScrolling = true;
        }
    },
    // Loading indicator functionality
    // There isn't really a particularly nice way to do this yet, so we just watch to see if
    // the expected number of pages are in the DOM yet.
    checkLoadingIndicator: function(list) {
        var scrollLeft = module.exports.vnode.dom.scrollLeft;
        var pageCount = module.exports.pages.childElementCount;
        var viewportWidth = document.body.clientWidth;
        if (pageCount * List.itemsPerPage * viewportWidth < scrollLeft && pageCount > 0) {
            module.exports.loadingIcon.classList.remove('hidden');
        } else {
            module.exports.loadingIcon.classList.add('hidden');
        }
    },
    // Initialize event listeners
    handler: function (vnode) {
        module.exports.vnode = vnode;
        module.exports.scrollPos = module.exports.vnode.dom.scrollLeft;
        module.exports.pages = module.exports.vnode.dom.querySelector('.mithril-infinite__pages');
        module.exports.loadingIcon = module.exports.viewVnode.dom.querySelector('.loading-icon');
        //Doesn't appear to be an easy way to find out when infinite scrolling is loading placeholders...
        var observer = new MutationObserver(module.exports.checkLoadingIndicator);
        observer.observe(module.exports.pages, {childList: true});
        // Scroll by mouse click works a bit differently from trackpad swiping.
        window.addEventListener('mousedown', function() {
            if (module.exports.scrollCallback) {
                window.cancelAnimationFrame(module.exports.scrollCallback.value);
            }
            if (module.exports.mouseupCallback) {
                window.cancelAnimationFrame(module.exports.mouseupCallback.value);
            }
            module.exports.isMouseScroll = true;
        });
        window.addEventListener('visibilitychange', module.exports.mouseGone);
        window.addEventListener('mouseup', module.exports.mouseGone);
        window.addEventListener('focus', module.exports.mouseGone);
        window.addEventListener('blur', module.exports.mouseGone);
        window.addEventListener('jumpToItem', function(e) {
            var index = e.detail;
            module.exports.vnode.dom.scrollTo(index * document.body.clientWidth, 0);
        });
        module.exports.vnode.dom.addEventListener('scroll', module.exports.scrollHandler, false);
        window.addEventListener('resize', function() {
            // Update static page widths of placeholders
            var pageSize = List.itemsPerPage * document.body.clientWidth;
            var keys = Object.keys(module.exports.vnode.state.pageSizes);
            var size = keys.length;
            for (var i = 0; i < size; i++) {
                module.exports.vnode.state.pageSizes[keys[i]] = pageSize;
            }
            module.exports.snapToItem();
        });

        module.exports.loadingIcon.classList.add('hidden');
    }
};
