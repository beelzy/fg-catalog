var m = require('mithril');
var RequestAnimation = require('../helper/RequestAnimation');
var FlashTrust = require('nw-flash-trust-a');
if (!window.webMode) {
    var fs = nw.require('fs');
    var nwFunctions = require('../helper/NWFunctions');
}
var Config = require('../models/Config');

module.exports = {
    ppapiPathRegex: /[^ ]*--ppapi-flash-path=([^ ]*)/g,
    qualityOptions: ['low', 'medium', 'high', 'best'],
    showValidation: false,
    validationText: '',
    newWidth: 0,
    newHeight: 0,
    newLaunchInWindow: false,
    newSWFPath: '',
    newDomainWhitelist: [],
    newPPAPIPath: '',
    newQualityIndex: 0,
    selectedQualityIndex: function() {
        var size = module.exports.qualityOptions.length;
        for (var i = 0; i < size; i++) {
            if (module.exports.qualityOptions[i] === Config.configs.quality) {
                return i;
            }
        }
        return -1;
    },
    resetConfigs: function() {
        if (Object.keys(Config.configs).length > 0) {
            module.exports.newWidth = Config.configs.windowsize.width;
            module.exports.newHeight = Config.configs.windowsize.height;
            module.exports.ppapiPathRegex.lastIndex = 0;
            module.exports.newLaunchInWindow = Config.configs.launchInWindow;
            module.exports.newSWFPath = Config.configs.swfPath;
            module.exports.newDomainWhitelist = Config.configs.allowDomains.join(',');
            module.exports.newPPAPIPath = module.exports.ppapiPathRegex.exec(Config.pkg['chromium-args'])[1];
            module.exports.newQualityIndex = module.exports.selectedQualityIndex();
        }
    },
    displayValidation: function(text) {
        module.exports.showValidation = true;
        module.exports.validationText = text;
        m.redraw();
        RequestAnimation.requestTimeout(function() {
            module.exports.showValidation = false;
            m.redraw();
        }, 1000);
    },
    localFlashTrust: undefined,
    appFlashTrust: undefined,
    initFlashTrust: function() {
        // Automatically trust flash locations
        var appName = 'fg-catalog';
        var customFolder = process.env.HOME + '/.config/fg-catalog/Default/Pepper Data/Shockwave Flash/WritableRoot';
        module.exports.appFlashTrust = FlashTrust.initSync(appName, {
            customFolder: customFolder,
            nwChromeExtensionsProtocol: true
        });
        module.exports.appFlashTrust.empty();
        module.exports.appFlashTrust.add('');

        if (Config.configs.hasOwnProperty('swfPath')) {
            module.exports.localFlashTrust = FlashTrust.initSync(appName, {
                customFolder: customFolder
            });
            module.exports.localFlashTrust.add('file://' + Config.configs.swfPath);
        }
    },
    saveHandler: function() {
        // Validate window dimensions
        var widthValue = parseInt(module.exports.newWidth);
        var heightValue = parseInt(module.exports.newHeight);

        if (
            isNaN(widthValue) || isNaN(heightValue)
            || widthValue < 1 || heightValue < 1
            || widthValue > window.screen.width || heightValue > window.screen.height
        ) {
            module.exports.newWidth = Config.configs.windowsize.width;
            module.exports.newHeight = Config.configs.windowsize.height;
            module.exports.displayValidation('Please enter a valid window width and height.');
            return;
        }

        Config.configs.windowsize.width = widthValue;
        Config.configs.windowsize.height = heightValue;

        nwFunctions.resizeWindow(Config.configs.windowsize);

        Config.configs.launchInWindow = module.exports.newLaunchInWindow;

        // Validate SWF root path
        if (!fs.existsSync(module.exports.newSWFPath)) {
            module.exports.newSWFPath = Config.configs.swfPath;
            module.exports.displayValidation('The SWF root path does not exist.');
            return;
        }

        module.exports.localFlashTrust.remove(Config.configs.swfPath);
        module.exports.localFlashTrust.add(module.exports.newSWFPath);

        Config.configs.swfPath = module.exports.newSWFPath;

        Config.configs.allowDomains = module.exports.newDomainWhitelist.split(',').map(function(address) {
            return address.trim();
        });

        // Validate PPAPI plugin path
        if (!fs.existsSync(module.exports.newPPAPIPath)) {
            module.exports.ppapiPathRegex.lastIndex = 0;
            module.exports.newPPAPIPath = module.exports.ppapiPathRegex.exec(Config.pkg['chromium-args'])[1];
            module.exports.displayValidation('The PPAPI plugin path does not exist.');
            return;
        }

        Config.pkg['chromium-args'] = Config.pkg['chromium-args'].replace(
            module.exports.ppapiPathRegex,
            '--ppapi-flash-path=' + module.exports.newPPAPIPath
        );

        Config.configs.quality = module.exports.qualityOptions[module.exports.newQualityIndex];

        var configsSaved = 0;

        var callback = function() {
            if (configsSaved === 2) {
                module.exports.displayValidation('saved!');
                dispatchEvent(new CustomEvent('saveComplete'));
            }
        }
        
        var ioErrorString = 'An IO error occurred while attempting to save.';

        fs.writeFile('data/config.json', JSON.stringify(Config.configs, null, 4), function(e) {
            if (e) {
                module.exports.displayValidation(ioErrorString);
            } else {
                configsSaved++;
                callback();
            }
        });

        fs.writeFile('package.json', JSON.stringify(Config.pkg, null, 2), function(e) {
            if (e) {
                module.export.displayValidation(ioErrorString);
            } else {
                configsSaved++;
                callback();
            }
        });
    },
    saveAndReload: function() {
        window.addEventListener('saveComplete', function() {
            chrome.runtime.reload();
        });
        module.exports.saveHandler();
    }
};
