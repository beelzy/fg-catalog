var m = require('mithril');
var data;
if (!window.webMode) {
    data = require('./data/dimensions.json');
} else {
    m.request({
        method: 'GET',
        url: 'data/dimensions.json',
        config: function(xhr) {
            xhr.overrideMimeType('application/json');
        }
    }).then(function(result) {
        Dimensions.keys = result;
        dispatchEvent(new CustomEvent('dataLoaded', {detail: 'dimensions'}));
    }).catch(function(e) {
        console.error(e);
        dispatchEvent(new CustomEvent('dataError', {detail: 'dimensions'}));
    });
}

var Dimensions = {
    keys: data
};

module.exports = Dimensions;
