var m = require('mithril');

var Tag = {
    list: {},
    init: function(data) {
        for (var key in data) {
            var item = data[key];
            var size = item.tags.length;
            for (var i = 0; i < size; i++) {
                var tag = item.tags[i];
                if (!Tag.list.hasOwnProperty(tag)) {
                    Tag.list[tag] = 0; 
                }
                Tag.list[tag] += 1;
            }
        }
    }
}

module.exports = Tag;
