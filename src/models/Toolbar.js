var m = require('mithril');
var Config = require('../models/Config');
var RequestAnimation = require('../helper/RequestAnimation');

module.exports = {
    showChromeWarning: function() {
        return Config.pkg['chrome-version'] !== process.versions.chromium;
    },
    showFlashWarning: function() {
        return !Config.isFlashSafe;
    },
    detectFlashVersion: function() {
        var dom = document.querySelector('.flashtest');
        if (dom && dom.GetVariable) {
            try {
            var version = dom.GetVariable('$version');
            var parts = version.split(',');
            var major = parseInt(parts[0].split(' ')[1]);
            Config.isFlashSafe = (major >= 32 && parseInt(parts[parts.length - 1]) <= 371) || major < 32;
            Config.flashVersion = major + '.' + parts.slice(1).join('.');
            } catch(e) {
                console.error(e);
                Config.isFlashSafe = false;
                Config.flashVersion = 'disabled';
            }
            m.redraw();
        } else {
            RequestAnimation.requestTimeout(function() {
                module.exports.detectFlashVersion();
            }, 10);
        }
    }
};
