var m = require('mithril');
var data;

if (!window.webMode) {
    try {
        data = require('./data/creators.json');
    } catch(e) {
        console.error('creators.json metadata is missing');
    }
} else {
    m.request({
        method: 'GET',
        url: 'data/creators.json',
        config: function(xhr) {
            xhr.overrideMimeType('application/json');
        }
    }).then(function(result) {
        Creator.list = result;
        dispatchEvent(new CustomEvent('dataLoaded', {detail: 'creators'}));
    }).catch(function(e) {
        console.error(e);
        dispatchEvent(new CustomEvent('dataError', {detail: 'creator'}));
    });
}

var Creator = {
    list: data || {}
}

module.exports = Creator;
