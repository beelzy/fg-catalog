var Config = require('./Config');
if (!window.webMode) {
    var nwFunctions = require('../helper/NWFunctions');
}
var Dimensions = require('./Dimensions');

module.exports = {
    currentState: 'catalog',
    currentGame: {},
    transitioning: false,
    preferredDimensions: [0, 0],
    preferredQuality: 'low',
    preferredUrl: '',
    embedType: 'swf',
    gameWindow: undefined,
    TOOLBAR_HEIGHT: 52,
    cropScreenDimensions: function(aspectRatio) {
        var viewportWidth = window.webMode ? window.innerWidth : window.screen.width;
        var viewportHeight = window.webMode ? window.innerHeight : window.screen.height;
        var screenHeight = Config.configs.launchInWindow ? viewportHeight : viewportHeight - module.exports.TOOLBAR_HEIGHT;
        var screenRatio = viewportWidth / screenHeight;
        if (aspectRatio > screenRatio) {
            module.exports.preferredDimensions[0] = viewportWidth;
            module.exports.preferredDimensions[1] = Math.round(viewportWidth / aspectRatio);
        } else {
            module.exports.preferredDimensions[1] = screenHeight;
            module.exports.preferredDimensions[0] = Math.round(screenHeight * aspectRatio);
        }
    },
    processDimensions: function(dimensions) {
        if (Array.isArray(dimensions)) {
            module.exports.preferredDimensions = dimensions;
        } else {
            module.exports.cropScreenDimensions(dimensions);
        }
    },
    calculateEmbedParams: function() {
        if (!window.webMode) {
            // Determine preferred dimensions from flash configs
            module.exports.preferredDimensions = Config.configs.hasOwnProperty('windowsize')
                ? [Config.configs.windowsize.width, Config.configs.windowsize.height]
                : [nwFunctions.DEFAULT_WIDTH, nwFunctions.DEFAULT_HEIGHT];
        } else {
            module.exports.preferredDimensions = [window.innerWidth, window.innerHeight];
        }
        if (module.exports.currentGame.hasOwnProperty('dimensions')) {
            var dimensions = module.exports.currentGame.dimensions;
            if (typeof dimensions === 'string') {
                if (dimensions !== 'default') {
                    module.exports.processDimensions(Dimensions.keys[dimensions]);
                }
            } else {
                module.exports.processDimensions(dimensions);
            }
        } else if (Dimensions.keys.hasOwnProperty(module.exports.currentGame.creator)) {
            var defaultDimensions = Dimensions.keys[module.exports.currentGame.creator];
            module.exports.processDimensions(defaultDimensions);
        }
        // Limit dimensions to under screen size while keeping aspect ratio
        var preferredHeight = Config.configs.launchInWindow ? module.exports.preferredDimensions[1] : module.exports.preferredDimensions[1] + module.exports.TOOLBAR_HEIGHT;
        if (module.exports.preferredDimensions[0] > window.screen.width || preferredHeight > window.screen.height) {
            var aspectRatio = module.exports.preferredDimensions[0] / preferredHeight;
            module.exports.cropScreenDimensions(aspectRatio);
        }
        module.exports.preferredQuality = module.exports.currentGame.hasOwnProperty('quality') ?
                module.exports.currentGame.quality
                : Config.configs.quality;
        module.exports.embedType = module.exports.currentGame.types.indexOf('browser') > -1 ? 'browser' : 'swf';
        module.exports.preferredUrl = module.exports.currentGame.entry.startsWith('http') ? module.exports.currentGame.entry : (window.webMode
            ? `${Config.configs.swfPath ? `${Config.configs.swfPath}/` : ''}${module.exports.currentGame.entry}`
            : 'file:///' + Config.configs.swfPath + module.exports.currentGame.entry);
    },
    launchGame: function() {
        if (module.exports.gameWindow) {
            module.exports.gameWindow.close();
        }

        if (Config.configs.launchInWindow) {
            nw.Window.open('embed.html', {resizable: false}, function(win) {
                module.exports.gameWindow = win;
                win.window.addEventListener('load', function() {
                    win.window.dispatchEvent(new CustomEvent('loadGame', {detail: module.exports}));
                });
            });
        } else {
            module.exports.currentState = 'game';
            module.exports.transitioning = true;
        }
    }
}
