var m = require('mithril');
var configData;
if (!window.webMode) {
    try {
        configData = require('./data/config.json');
    } catch(e) {
        console.error('config.json metadata is missing');
    }
    var pkgData = require('./package.json');
} else {
    var mimeType = function(xhr) {
        xhr.overrideMimeType('application/json');
    };
    m.request({
        method: 'GET',
        url: 'data/config.json',
        config: mimeType
    }).then(function(result) {
        Config.configs = result;
        dispatchEvent(new CustomEvent('dataLoaded', {detail: 'config'}));
    }).catch(function(e) {
        console.error(e);
        dispatchEvent(new CustomEvent('dataError', {detail: 'config'}));
    });
    m.request({
        method: 'GET',
        url: 'package.json',
        config: mimeType
    }).then(function(result) {
        Config.pkg = result;
        dispatchEvent(new CustomEvent('dataLoaded', {detail: 'pkg'}));
    }).catch(function(e) {
        console.error(e);
        dispatchEvent(new CustomEvent('dataError', {detail: 'pkg'}));
    });
}

var Config = {
    configs: configData || {},
    pkg: pkgData,
    isFlashSafe: true,
    flashVersion: 0
};

module.exports = Config;
