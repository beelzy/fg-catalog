var m = require('mithril');
var Tag = require('./Tag');
var data;
if (!window.webMode) {
    try {
        data = require('./data/catalog.json');
        Tag.init(data);
    } catch(e) {
        console.error('catalog.json metadata is missing');
    }
} else {
    m.request({
        method: 'GET',
        url: 'data/catalog.json',
        config: function(xhr) {
            xhr.overrideMimeType('application/json');
        }
    }).then(function(result) {
        List.list = result;
        List.keys = Object.keys(result).sort();
        Tag.init(result);
        dispatchEvent(new CustomEvent('dataLoaded', {detail: 'list'}));
    }).catch(function(e) {
        console.error(e);
        dispatchEvent(new CustomEvent('dataError', {detail: 'list'}));
    });
}

var List = {
    list: data || {},
    keys: Object.keys(data || {}).sort(),
    itemsPerPage: 25,
    ascending: true,
    sortBy: 'title',
    sort: function(key) {
        List.sortBy = key;
        if (key === 'title') {
            List.keys.sort();
        } else {
            List.keys.sort(function (a, b) {
                var itemA = List.list[a];
                var itemB = List.list[b];
                var compareA = itemA[key];
                var compareB = itemB[key];
                if (compareA < compareB) {
                    return -1;
                }
                if (compareA > compareB) {
                    return 1;
                }
                return 0;
            });
        }

        if (!List.ascending) {
            List.keys.reverse();
        }
    },
    filterKeys: function(filter, value) {
        var keys = [];
        List.keys.map(function(key) {
            var item = List.list[key];
            if (filter === 'tag' && item.tags.indexOf(value) < 0) {
                return;
            }
            if (filter === 'creator' && item.creator !== value) {
                return;
            }
            keys.push(key);
        });
        return keys;
    },
    countFilteredItems: function(filter, value) {
        var count = 0;
        List.keys.map(function(key) {
            var item = List.list[key];
            if (filter === 'tag' && item.tags.indexOf(value) < 0) {
                return;
            }
            if (filter === 'creator' && item.creator !== value) {
                return;
            }
            count++;
        });
        return count;
    }
};

module.exports = List;
