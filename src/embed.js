var m = require('mithril');

var Embed = require('./views/Embed');
var State = require('./models/State');
var Creator = require('./models/Creator');
var nwFunctions = require('./helper/NWFunctions');

var REMOTE_SIZE = [200, 320];
var remoteWindow;

var positionRemote = function() {
    var maxX = window.screen.availLeft + window.screen.width - 200;
    var x = Math.min(nwFunctions.win.x + nwFunctions.win.width, maxX);
    remoteWindow.moveTo(x, remoteWindow.y);
}

nwFunctions.initWindowPolicy();

window.addEventListener('loadGame', function(e) {
    document.title = e.detail.currentGame.title + ' - ' + Creator.list[e.detail.currentGame.creator].name;
    nwFunctions.resizeWindow({
        width: e.detail.preferredDimensions[0],
        height: e.detail.preferredDimensions[1]
    });
    m.render(document.body, m('.headless.game', m(Embed, {state: e.detail})));
    if (remoteWindow) {
        positionRemote();
    }
});

nw.Window.open('remote.html', {width: 200, height: 320, resizable: false}, function(win) {
    remoteWindow = win;
    positionRemote();
    nwFunctions.win.on('close', function() {
        win.close(true);
        this.close(true);
    });
});

window.addEventListener('message', function(e) {
    if (e.origin === window.location.origin) {
        if (e.data === 'Rewind') {
            document.querySelector('object').Rewind();
        } else {
            document.querySelector('object').Play();
        }
    } else {
        console.error('Warning: postmessage() ' + e.origin + ' origin does not match.');
    }
});


