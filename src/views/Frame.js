var m = require('mithril');
var State = require('../models/State');
var Config = require('../models/Config');
var FlashVersionTest = require('./FlashVersionTest');
var List = require('./List');
var Embed = require('./Embed');
var Toolbar = require('./Toolbar');
var Tabs = require('./Tabs');

module.exports = {
    view: function(vnode) {
        return [
            (Config.flashVersion === 0 ? m(FlashVersionTest) : ''),
            m('.fg-viewer-wrapper'
            + (State.currentState === 'game' ? '.game' : '')
            + (State.transitioning ? '.transition' : ''), [
            m(Toolbar, {routeInfo: vnode.attrs.routeInfo}),
            m(List, Object.assign({}, {loading: vnode.attrs.loading},  vnode.attrs.routeInfo)),
            m(Embed),
            m(Tabs, {routeInfo: vnode.attrs.routeInfo})
        ])
        ]
    }
};
