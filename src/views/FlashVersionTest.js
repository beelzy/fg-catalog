var m = require('mithril');
var Toolbar = require('../models/Toolbar');
var Config = require('../models/Config');

module.exports = {
    oninit: function(vnode) {
        Toolbar.detectFlashVersion();
    },
    view: function(vnode) {
        return m('object.flashtest', {
            type: 'application/x-shockwave-flash',
            //Chromium refuses to load swf files if they're smaller than 6 pixels.
            width: 6,
            height: 6,
            data: 'blank.swf'
        },
            [
                m('param', {
                    name: 'movie',
                    value: 'blank.swf'
                }),
                m('param', {name: 'allowscriptaccess', value: 'always'})
            ]
        );
    }
};
