var m = require('mithril');
if (!window.webMode) {
    var nwFunctions = require('../helper/NWFunctions');
}
var SVG = require('../helper/SVG');
var Controls = require('./Controls');
var Config = require('../models/Config');
var Creator = require('../models/Creator');
var State = require('../models/State');

module.exports = {
    oncreate: function(vnode) {
        vnode.dom.addEventListener('transitionend', function() {
            State.transitioning = false;
            if (!window.webMode) {
                if (State.currentState === 'game') {
                    nwFunctions.resizeWindow(
                        {
                            width: State.preferredDimensions[0],
                            height: State.preferredDimensions[1] + State.TOOLBAR_HEIGHT
                        }
                    );
                    m.redraw();
                    console.info('play game', State.currentGame);
                } else {
                    nwFunctions.resizeWindow(Config.configs.windowsize);
                }
            } else {
                m.redraw();
            }
        });
    },
    view: function(vnode) {
        var title = State.currentGame.hasOwnProperty('title') ? State.currentGame.title : '';
        var creator = State.currentGame.hasOwnProperty('creator') ? Creator.list[State.currentGame.creator].name : '';
        return m('.game-controls', [
            m('.nav', m('a.tag-button', {
                onclick: function() {
                    State.currentState = 'catalog';
                }
            }, [
                m('span.circle-left', SVG.icon('times')),
                m('span.title', 'Back to Catalog')
            ])),
            m(Controls),
            m('.title', [
                m('h3', {title: title}, title),
                m('span', {title: creator}, creator)
            ])
        ])
    }
};
