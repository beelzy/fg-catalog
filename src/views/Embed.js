var m = require('mithril');
var State = require('../models/State');
var Config = require('../models/Config');

if (!window.webMode) {
    var path = nw.require('path');
}

module.exports = {
    oninit: function(vnode) {
        if (vnode.attrs.hasOwnProperty('state')) {
            State.currentGame = vnode.attrs.state.currentGame;
            State.preferredDimensions = vnode.attrs.state.preferredDimensions;
            State.preferredQuality = vnode.attrs.state.preferredQuality;
            State.preferredUrl = vnode.attrs.state.preferredUrl;
            State.embedType = vnode.attrs.state.embedType;
            State.currentState = 'game';
        }
    },
    view: function(vnode) {
        if (
            State.currentState === 'game'
            && !State.transitioning
            && State.currentGame
            && Object.keys(State.currentGame).length > 0
        ) {
            if (State.embedType === 'swf') {
                const pathIndex = State.currentGame.entry.lastIndexOf('/');
                return m('.embed-wrapper', 
                    m('object', {
                        type: 'application/x-shockwave-flash',
                        width: State.preferredDimensions[0],
                        height: State.preferredDimensions[1],
                        data: State.preferredUrl
                    },
                        [
                            m('param', {
                                name: 'movie',
                                value: State.preferredUrl
                            }),
                            m('param', {
                                name: 'base',
                                value: window.webMode
                                ? `${Config.configs.swfPath ? `${Config.configs.swfPath}/` : ''}${pathIndex > 0 ? State.currentGame.entry.slice(0, pathIndex) : ''}/`
                                    : path.dirname(
                                    'file:///' + Config.configs.swfPath + State.currentGame.entry
                                ) + '/'
                            }),
                            m('param', {name: 'quality', value: State.preferredQuality}),
                            m('param', {name: 'scale', value: 'default'}),
                            m('param', {name: 'wmode', value: 'direct'}),
                            m('param', {name: 'allowscriptaccess', value: 'always'})
                        ]
                    )
                )
            } else {
                return m('.embed-wrapper', m('iframe#browser-embed', {
                    src: State.preferredUrl,
                    width: State.preferredDimensions[0],
                    height: State.preferredDimensions[1]
                }))
            }
        }
        return;
    }
};
