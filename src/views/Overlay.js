var m = require('mithril');

module.exports = {
    view: function(vnode) {
        return m('.overlay' + (vnode.attrs.hasOwnProperty('class') ? '.' + vnode.attrs.class : ''),
            m('.info-box', vnode.attrs.content)
        );
    }
};
