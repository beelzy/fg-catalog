var m = require('mithril');
var infinite = require('mithril-infinite');
var State = require('../models/State');
var List = require('../models/List');
var Creator = require('../models/Creator');
var Config = require('../models/Config');
var infiniteScroll = require('../helper/InfiniteScroll');
var ListEventsHandler = require('../helper/ListEventsHandler');
var SVG = require('../helper/SVG');

module.exports = {
    view: function(viewVnode) {
        var hasConfigs = Object.keys(Config.configs).length > 0 && Config.configs.hasOwnProperty('symbols');
        var hasList = List.keys.length > 0;
        var hasCreators = Object.keys(Creator.list).length > 0;
        var itemRender = function (data) {
            if (!data) {
                return;
            }

            return m('.game-item', m('.container', [
                m('h1', {title: data.item.title}, data.item.title),
                m('.creator', m('.tag-button.creator', [
                    m('a.title', {
                        oncreate: m.route.link, href: '/creator/' + data.item.creator
                    }, data.creator.name),
                    data.creator.url
                    ? m('a.circle-creator', {
                        href: data.creator.url,
                        title: data.creator.url,
                        onclick: function(e) {
                            e.preventDefault();
                            nw.Shell.openExternal(data.creator.url);
                        }
                    }, SVG.icon('link'))
                    : m('a.circle-creator.hidden')
                ])),
                m('.description', data.item.description),
                m('.difficulty', [
                    m('.title', 'Difficulty'),
                    m('.score-wrapper', [
                        m('.bars', data.bars),
                        m('h1', data.item.difficulty)
                    ])
                ]),
                m('span.notes', [
                    m(data.item.notes
                        ? 'a[href=' + Config.configs.notes + data.item.notes + ']'
                        : 'span', data.item.notes
                        ? {
                            href: Config.configs.notes + data.item.notes,
                            title: data.item.notes,
                            onclick: function(e) {
                                e.preventDefault();
                                nw.Shell.openExternal(Config.configs.notes + data.item.notes);
                            }
                        }
                        : {}, 'Notes'),
                    m('span.types-wrapper', hasConfigs ? (data.item.types.length > 0
                        ? data.item.types.map(function(key) {
                        var symbol = Config.configs.symbols[key];
                        return SVG.template(
                            symbol.icon.src,
                            symbol.icon.id,
                            symbol.icon.size,
                            null,
                            symbol.title);
                    })
                        : SVG.template(
                            data.complete.icon.src,
                            data.complete.icon.id,
                            data.complete.icon.size,
                            null,
                            data.complete.title
                        )) : undefined
                    )
                ]),
                m('.entry', [
                    SVG.icon('sign-in'),
                    m('span', data.item.entry),
                    m('div.play', m('a.tag-button', {onclick: function() {
                        State.currentGame = data.item;
                        State.calculateEmbedParams();

                        State.launchGame();
                    }}, [
                        m('span.title', 'Play'),
                        m('span.circle', SVG.icon('play-o'))
                    ]))
                ]),
                m('.tags', data.item.tags.map(function(tag) {
                    return m('a.tag-button', { oncreate: m.route.link, href: '/tag/' + tag }, tag);
                }))
            ]))
        };
        ListEventsHandler.viewVnode = viewVnode;
        var oncreateInfinite = ListEventsHandler.handler;
        var generateList = function() {
            if (hasList && hasCreators) {
                var filteredKeys = List.filterKeys(viewVnode.attrs.route, viewVnode.attrs.id);
                var totalItems = List.countFilteredItems(viewVnode.attrs.route, viewVnode.attrs.id);
                var dataFn = function (j) {
                    var key = filteredKeys[j];
                    var item = List.list[key];
                    var creator = Creator.list[item.creator];
                    var complete = hasConfigs ? Config.configs.symbols.complete : undefined;
                    var bars = [];
                    for (var i = 0; i < item.difficulty; i++) {
                        bars.push(m('.bar'));
                    }
                    return {item: item, creator: creator, bars: bars, complete: complete};
                };
                var infiniteOptions = infiniteScroll.createOptions({
                    count: totalItems,
                    itemsPerPage: List.itemsPerPage,
                    itemRender: itemRender,
                    dataFn: dataFn
                });
                infiniteOptions.oncreate = oncreateInfinite;
                infiniteOptions.contentSize = totalItems * document.body.clientWidth;
                infiniteOptions.class = 'fg-infinite-scroll';
                infiniteOptions.pageKey = function(pageNum) {
                    return pageNum
                        + '-' + viewVnode.attrs.route
                        + '-' + viewVnode.attrs.id
                        + ':' + List.sortBy
                        + '-' + (List.ascending ? 'inc' : 'dec');
                };
                infiniteOptions.axis = 'x';

                return m(infinite, infiniteOptions)
            } else if(!viewVnode.attrs.loading) {
                var missingList = hasList ? '' : 'Error: catalog.json metadata is missing.';
                var missingCreators = hasCreators ? '' : 'Error: creators.json metadata is missing.';
                return m('.error', [m('div', missingList), m('div', missingCreators)]);
            }
        };
        return m('.games-wrapper', [
            m('.bg', SVG.icon('flash')),
            m('.loading-icon'
                + (!hasList || !hasCreators ? '.hidden' : ''),
                m('div', [m('div', 'Loading...'), m('img', {src: 'icons/puff.svg'})])),
            generateList()
        ]);
    }
};
