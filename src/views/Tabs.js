var m = require('mithril');
var Tabs = require('../models/Tabs');
var Index = require('./tabs/Index');
var Creators = require('./tabs/Creators');
var Tags = require('./tabs/Tags');
var SVG = require('../helper/SVG');

module.exports = {
    view: function(vnode) {
        var routeChanged = (Tabs.currentRoute ? '/' + Tabs.currentRoute + '/' + Tabs.currentId : '/')
            !== decodeURI(m.route.get());
        Tabs.currentId = vnode.attrs.routeInfo.hasOwnProperty('id') ? vnode.attrs.routeInfo.id : '';
        Tabs.currentRoute = vnode.attrs.routeInfo.hasOwnProperty('route') ? vnode.attrs.routeInfo.route : '';
        if (routeChanged) {
            Tabs.classes.index = '.selected';
            Tabs.classes.tags = '';
            Tabs.classes.creators = '';
        }
        return m('.filters-wrapper', [
            m('.tabs', [
                m('h3.tab-title' + Tabs.classes.index, { onclick: function() {
                    Tabs.classes.creators = '';
                    Tabs.classes.tags = '';
                    Tabs.classes.index = '.selected';
                } }, [SVG.icon('list'), m('span', 'Index')]),
                m('h3.tab-title' + Tabs.classes.creators, { onclick: function() {
                    Tabs.classes.creators = '.selected';
                    Tabs.classes.tags = '';
                    Tabs.classes.index = '';
                } }, [SVG.icon('user'), m('span', 'Creators')]),
                m('h3.tab-title' + Tabs.classes.tags, { onclick: function() {
                    Tabs.classes.creators = '';
                    Tabs.classes.tags = '.selected';
                    Tabs.classes.index = '';
                } }, [SVG.icon('tags'), m('span', 'Tags')])
            ]),
            m('.tab-panel', [
                m('.tab-wrapper.index' + Tabs.classes.index, m(Index, vnode.attrs.routeInfo)),
                m('.tab-wrapper' + Tabs.classes.creators, m(Creators)),
                m('.tab-wrapper' + Tabs.classes.tags, m(Tags))
            ])
        ])
    }
};
