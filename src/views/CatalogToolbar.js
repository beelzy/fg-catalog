var m = require('mithril');

var List = require('../models/List');
var Creator = require('../models/Creator');
var SVG = require('../helper/SVG');

module.exports = {
    view: function(vnode) {
        return m('.catalog', [
            m('.current-filter', [
                m('h3.current-filter-title', [
                    'Filter',
                    m('span.count',
                        List.countFilteredItems(vnode.attrs.routeInfo.route, vnode.attrs.routeInfo.id)
                        + ' game(s)'
                    )
                ]),
                (vnode.attrs.routeInfo.route !== undefined)
                ? m('a.tag-button.' + vnode.attrs.routeInfo.route,
                    {oncreate: m.route.link, href: '/' },
                    [
                        m('span.circle-left', vnode.attrs.routeInfo.route === 'tag'
                            ? SVG.icon('tag')
                            : SVG.icon('user')
                        ),
                        m('span.title', vnode.attrs.routeInfo.route === 'creator'
                            ? Creator.list[vnode.attrs.routeInfo.id]?.name
                            : vnode.attrs.routeInfo.id
                        ),
                        SVG.icon('times')
                    ])
                : null
            ]),
            m('.sort-filter', [
                m('h3.sort-filter-title', 'Sort'),
                m('.sort-options', [
                    m('select.sort', { onchange: function(e) {
                        List.sort(e.target.value);
                    } },
                        [
                            m('option[value=title]', 'Title'),
                            m('option[value=creator]', 'Creator'),
                            m('option[value=difficulty]', 'Difficulty')
                        ]),
                    m('.sort-order' + (List.ascending ? '' : '.reverse'),
                        {onclick: function() {
                            List.ascending = !List.ascending;
                            List.keys.reverse();
                        }},  List.ascending
                        ? SVG.icon('sort-asc')
                        : SVG.icon('sort-desc')
                    )
                ])
            ])
        ])
    }
};
