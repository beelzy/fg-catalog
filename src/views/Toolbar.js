var m = require('mithril');
var Config = require('../models/Config');
var Toolbar = require('../models/Toolbar');
var CatalogToolbar = require('./CatalogToolbar');
var GameToolbar = require('./GameToolbar');
var Info = require('./overlays/Info');
var FlashWarning = require('./overlays/FlashWarning');
if (!window.webMode) {
    var ChromeWarning = require('./overlays/ChromeWarning');
    var Settings = require('./overlays/Settings');
}
var SVG = require('../helper/SVG');

module.exports = {
    view: function(vnode) {
        return m('.toolbar', [
            m(CatalogToolbar, {routeInfo: vnode.attrs.routeInfo}),
            m(GameToolbar),
            m('span.info', [
                m('a.icon-button.info', SVG.icon('question')),
                (window.webMode ? [] : m('a.icon-button.settings', SVG.icon('settings'))),
                (Config.isFlashSafe ? [] : m('a.icon-button.flash-warning' + (Toolbar.showFlashWarning()
                    ? ''
                    : '.hidden'),
                    SVG.icon('flash')
                )),
                (window.webMode ? [] : m('a.icon-button.chrome-warning' + (Toolbar.showChromeWarning()
                    ? ''
                    : '.hidden'),
                    SVG.icon('chrome')
                )),
                m(Info),
                (window.webMode ? [] : m(Settings)),
                (Config.isFlashSafe ? [] : m(FlashWarning)),
                (window.webMode ? [] : m(ChromeWarning))
            ])
        ])
    }
};
