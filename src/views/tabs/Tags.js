var m = require('mithril');
var infinite = require('mithril-infinite');
var infiniteScroll = require('../../helper/InfiniteScroll');
var Tag = require('../../models/Tag');

var itemsPerPage = 50;

module.exports = {
    view: function() {
        var tagKeys = Object.keys(Tag.list);
        var tagCount = tagKeys.length;
        var infiniteOptions = infiniteScroll.createOptions({
            count: tagCount,
            itemsPerPage: itemsPerPage,
            itemRender: function(tag) {
                return m('a.tag-button', { oncreate: m.route.link, href: '/tag/' + tag.key }, [
                    tag.key,
                    m('span.circle-expandable', tag.count)
                ]);
            },
            dataFn: function(j) {
                var key = tagKeys[j];
                var item = Tag.list[key];
                return {key: key, count: item};
            }
        });
        return m(infinite, infiniteOptions);
    }
};
