var m = require('mithril');
var infinite = require('mithril-infinite');
var infiniteScroll = require('../../helper/InfiniteScroll');
var List = require('../../models/List');

var itemsPerPage = 50;

module.exports = {
    view: function(vnode) {
        var filteredKeys = List.filterKeys(vnode.attrs.route, vnode.attrs.id);
        var totalItems = List.countFilteredItems(vnode.attrs.route, vnode.attrs.id);
        var infiniteOptions = infiniteScroll.createOptions({
            count: totalItems,
            itemsPerPage: itemsPerPage,
            itemRender: function (item) {
                return m('a.tag-button', { onclick: function(e) {
                    window.dispatchEvent(new CustomEvent('jumpToItem', {detail: item.index}));
                } }, [m('.circle-expandable.circle-left', item.index + 1), item.title]);
            },
            dataFn: function(j) {
                var key = filteredKeys[j];
                var item = List.list[key];
                return {index: j, title: item.title};
            }
        });
        infiniteOptions.pageKey = function(pageNum) {
            return pageNum
                + '-' + vnode.attrs.route
                + '-' + vnode.attrs.id
                + ':' + List.sortBy
                + '-' + (List.ascending ? 'inc' : 'dec');
        };

        return m(infinite, infiniteOptions);
    }
};
