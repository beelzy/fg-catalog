var m = require('mithril');
var infinite = require('mithril-infinite');
var infiniteScroll = require('../../helper/InfiniteScroll');
var Creator = require('../../models/Creator');
var SVG = require('../../helper/SVG');

var itemsPerPage = 50;

module.exports = {
    view: function() {
        var creatorCount = Object.keys(Creator.list).length;
        var sortedKeys = Object.keys(Creator.list).sort();
        var infiniteOptions = infiniteScroll.createOptions({
            count: creatorCount,
            itemsPerPage: itemsPerPage,
            itemRender: function (creator) {
                return m('span.tag-button.creator', [
                    m('a.title', {oncreate: m.route.link, href: '/creator/' + creator.key}, creator.name),
                    creator.url
                    ? m('a.circle-creator',
                        {
                            href: creator.url,
                            title: creator.url,
                            onclick: function(e) {
                                e.preventDefault();
                                nw.Shell.openExternal(creator.url);
                            }
                        },
                        SVG.icon('link')
                    )
                    : m('a.circle-creator.hidden')
                ])
            },
            dataFn: function(j) {
                var key = sortedKeys[j];
                var creator = Creator.list[key];
                return {key: key, url: creator.url, name: creator.name};
            }
        });
        return m(infinite, infiniteOptions);
    }
};
