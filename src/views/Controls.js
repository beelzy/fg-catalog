var m = require('mithril');
var SVG = require('../helper/SVG');
var Controls = require('../models/Controls');

module.exports = {
    oninit: function(vnode) {
        if (vnode.attrs.hasOwnProperty('remote')) {
            Controls.isRemote = vnode.attrs.remote;
        }
    },
    view: function(vnode) {
        return m('.flash-controls', [
            m('a.tag-button', {onclick: function() {
                if (Controls.isRemote) {
                    window.opener.postMessage('Rewind', window.location.origin);
                } else {
                    document.querySelector('object').Rewind();
                }
            }, title: 'Rewind'}, SVG.icon('reset')),
            m('a.tag-button', {onclick: function() {
                if (Controls.isRemote) {
                    window.opener.postMessage('Play', window.location.origin);
                } else {
                    document.querySelector('object').Play();
                }
            }, title: 'Play'}, SVG.icon('play'))
        ]);
    }
}
