var m = require('mithril');
var Config = require('../../models/Config');
var Settings = require('../../models/Settings');
var Overlay = require('../Overlay');
var Utils = require('../../helper/Utils');
var SVG = require('../../helper/SVG');

module.exports = {
    view: function(vnode) {
        if (Object.keys(Config.configs).length > 0) {
            if (Settings.newWidth === 0) {
                Settings.resetConfigs();
            }

            return m(Overlay, {class: 'settings', content: [
                m('.header', [
                    m('div.icon', SVG.icon('settings')),
                    m('.title', [
                        m('h1', 'Settings'),
                        m('span.validation' + (Settings.showValidation ? '' : '.hidden'),
                            Settings.validationText
                        ),
                        m('span.subtitle',
                            'Settings that require a restart will not take effect until the app restarts.'
                        )
                    ]),
                ]),
                m('div.pane', [m('h3', 'Application Settings'),
                    m('ul', [
                        m('li', [
                            m('div', 'Default window size:'),
                            m('div.control', [
                                m('input.dimensions', {
                                    min: 1,
                                    max: window.screen.width,
                                    type: 'number',
                                    placeholder: 'width',
                                    value: Settings.newWidth,
                                    onchange: function(e) {
                                        Settings.newWidth = e.currentTarget.value;
                                    }
                                }),
                                'px x ',
                                m('input.dimensions', {
                                    min: 1,
                                    max: window.screen.height,
                                    type: 'number',
                                    placeholder: 'height',
                                    value: Settings.newHeight,
                                    onchange: function(e) {
                                        Settings.newHeight = e.currentTarget.value;
                                    }
                                }),
                                'px'
                            ])
                        ]),
                        m('li', m('div.control.checkbox', [
                            m('input', {
                                type: 'checkbox',
                                checked: Settings.newLaunchInWindow ? 'checked' : false,
                                onchange: function(e) {
                                    Settings.newLaunchInWindow = e.currentTarget.checked;
                                }
                            }),
                            m('label', 'Launch games in a separate window')
                        ])),
                        m('li', [
                            m('div', 'SWF root path:'),
                            m('div.control', m('input.path', {
                                placeholder: '/Path/to/where/swf/files/reside',
                                value: Settings.newSWFPath,
                                onchange: function(e) {
                                    Settings.newSWFPath = e.currentTarget.value;
                                }
                            }))
                        ]),
                        m('li', [
                            m('div', {
                                title:'If any of your flash games depends on a LAN server, whitelist their domains here so that fg-catalog does not block them.'
                            }, 'LAN domain whitelist:'),
                            m('div.control', m('input.path', {
                                placeholder: 'example.com,somedomain.home,etc.',
                                value: Settings.newDomainWhitelist,
                                onchange: function(e) {
                                    Settings.newDomainWhitelist = e.currentTarget.value;
                                }
                            }))
                        ])
                    ]),
                    m('h3', 'NW.js Settings'),
                    m('ul', [
                        m('li', [
                            m('div', 'PPAPI Plugin path (requires restart):'),
                            m('div.control', m('input.path', {
                                placeholder: '/Path/to/Plugin/libpepflashplayer.so',
                                value: Settings.newPPAPIPath,
                                onchange: function(e) {
                                    Settings.newPPAPIPath = e.currentTarget.value;
                                }
                            }))
                        ])
                    ]),
                    m('h3', 'Flash Settings'),
                    m('ul', [
                        m('li', ['Current Version: ', m('span' + (Config.isFlashSafe ? '' : '.warning'), Config.flashVersion)]),
                        m('li', [
                            m('span', 'Quality:'),
                            m('select.quality', {
                                selectedIndex: Settings.newQualityIndex,
                                onchange: function(e) {
                                    Settings.newQualityIndex = e.currentTarget.selectedIndex;
                                }
                            }, Settings.qualityOptions.map(function(option) {
                                return m('option[value=i' + option + ']', Utils.capitalize(option));
                            }))
                        ])
                        // Unfortunately, this doesn't quite work yet. The settings manager displays, but doesn't actually save anything. It depends on a very specific SharedObject stored in the macromedia.com domain,
                        // which probably explains why the global settings dialog has to be accessed from the macromedia.com website.
                        // Tried with editing hosts file, but it doesn't work either.
                        // No sharedobject actually gets created.
                        /*
                        m('li', m('a.tag-button', {
                            onclick: function() {
                                if (Settings.globalWin) {
                                    Settings.globalWin.close(true);
                                }
                                nw.Window.open('http://macromedia.com:7006/settingsmanager/index.html', {resizable: false, width: 395, height: 270}, function(win) {
                                    Settings.globalWin = win;
                                });
                            }
                        }, [
                            m('span.circle-left', SVG.icon('globe')),
                            m('span.title', 'Global Settings')
                        ])),
                        m('li', m('a.tag-button', {
                            onclick: function() {
                                if (Settings.globalWin) {
                                    Settings.globalWin.close(true);
                                }
                                nw.Window.open('file://' + path.dirname(process.execPath) + '/package.nw/settingsmanager/settings_manager05.html', {resizable: false, width: 395, height: 270}, function(win) {
                                    Settings.globalWin = win;
                                });
                            }
                        }, [
                            m('span.circle-left', SVG.icon('globe')),
                            m('span.title', 'Global Auto-Notification Settings')
                        ]))*/
                    ])
                ]),
                m('div.buttons', [
                    m('a.tag-button', {
                        onclick: function() {
                            Settings.resetConfigs();
                            m.redraw();
                        }
                    }, [
                        m('span.circle-left', SVG.icon('reset')),
                        m('span.title', 'Reset')
                    ]),
                    m('a.tag-button', {onclick: Settings.saveHandler}, [
                        m('span.circle-left', SVG.icon('save')),
                        m('span.title', 'Save')
                    ]),
                    m('a.tag-button', {onclick: Settings.saveAndReload}, [
                        m('span.circle-left', SVG.icon('reload')),
                        m('span.title', 'Save & Restart')
                    ])
                ])
            ]})
        }
        return m(Overlay, {class: 'settings', content:
            m('.header', [
                m('div.icon', SVG.icon('settings')),
                m('.title', [
                    m('h1', 'Settings'),
                    m('span.subtitle',
                        'Error: configs.json metadata is missing. You will not be able to run any games in fg-catalog.'
                    )
                ]),
            ])
        });
    }
};
