var m = require('mithril');

var SVG = require('../../helper/SVG');
var Config = require('../../models/Config');
var Overlay = require('../Overlay');

module.exports = {
    view: function(vnode) {
        return m(Overlay, {class: 'info', content: [
            m('.header', [
                SVG.icon('flash'),
                m('.title', [
                    m('h1', 'Flash Games Catalog'),
                    m('span.version', 'v' + (Config.pkg ? Config.pkg.version : '') + ' | GPLv3+'),
                    m('span.subtitle', [
                        'Made with ',
                        m('a', {
                            href: 'https://mithril.js.org',
                            title: 'https://mithril.js.org',
                            onclick: function(e) {
                                e.preventDefault();
                                nw.Shell.openExternal('https://mithril.js.org');
                            }
                        }, 'Mithril'),
                        ' and ',
                        m('a', {
                            href: 'https://nwjs.io',
                            title: 'https://nwjs.io',
                            onclick: function(e) {
                                e.preventDefault();
                                nw.Shell.openExternal('https://nwjs.io');
                            }
                        }, 'NW.js'),
                        '.'
                    ])
                ]),
            ]),
            m('h3', 'Symbols'),
            m('ul.symbol-list', Config.configs.hasOwnProperty('symbols')
                ? Object.keys(Config.configs.symbols).map(function(key) {
                    var item = Config.configs.symbols[key];
                    return m('li.' + key, [
                        SVG.template(item.icon.src, item.icon.id, item.icon.size),
                        m('.title', item.title),
                        m('.description', item.description)
                    ])
                })
                : '')
        ]})
    }
};
