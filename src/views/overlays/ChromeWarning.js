var m = require('mithril');

var Config = require('../../models/Config');
var Overlay = require('../Overlay');
var SVG = require('../../helper/SVG');

module.exports = {
    view: function(vnode) {
        return m(Overlay, {class: 'chrome-warning', content: [
            m('.header', [
                m('div.icon', SVG.icon('chrome')),
                m('.title', [
                    m('h1', 'CHROMIUM VERSION WARNING'),
                    m('span.subtitle', [
                        'Chromium version should be ',
                        m('b', Config.pkg['chrome-version']),
                        ', but is actually ',
                        m('b', process.versions.chromium), '.'
                    ])
                ]),
            ]),
            m('h3', 'The version of the Chromium driver included in this app has changed.'),
            m('p', m('i', 'Why is this happening?')),
            m('p', 'NW.js apps ship with a specific version of the Chromium driver, and fg-catalog is designed to use a static version of Chromium to ensure continuing compatibility with the flash PPAPI plugin. Therefore, no autoupdate libraries or configurations are included with fg-catalog, and the Chromium version should theoretically not go up. If for some reason, this is still happening, then something unexpected has happened with the way NW.js works, and Chromium is still being updated. To prevent this, please contact the developers of NW.js.'),
            m('p', 'Another possibility is that the Chromium driver is a lower version than the target one, since fg-catalog only checks if the version is different.')
        ]})
    }
};
