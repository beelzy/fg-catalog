var m = require('mithril');

var Config = require('../../models/Config');
var Overlay = require('../Overlay');
var SVG = require('../../helper/SVG');

module.exports = {
    view: function(vnode) {
        return m(Overlay, {class: 'flash-warning', content: [
            m('.header', [
                m('div.icon', SVG.icon('flash')),
                m('.title', [
                    m('h1', 'FLASH VERSION WARNING'),
                    m('span.subtitle', [
                        'Flash version should be ',
                        m('b', '32.0.0.371'),
                        ' or lower, but is actually ',
                        m('b', Config.flashVersion),
                        '.'
                    ])
                ])
            ]),
            m('h3', 'You are using a version of Pepper Flash that will no longer function properly after January 12, 2021.'),
            m('p', m('i', 'Why is this happening?')),
            m('p', 'Adobe placed a kill switch in flash players above version 32.0.0.371, so that when the date on your computer is January 12th, 2021 or later, the flash player plugin used in this catalog will not work at all. Unfortunately, Adobe is aggressively trying to prevent users from obtaining a version of the flash plugin that will continue to work after this date, and you will no longer be able to obtain copies of older versions of the player from their website.'),
            m('p', m('i', 'What you can do')),
            m('p', 'If you\'re on Linux, which the main target audience of this catalog is intended for, your package manager of your distro probably has archives of an older package of the pepper flash plugin somewhere, or may not even have upgraded yet if their stable repos are LTS. Also, Archive.org may have some, but they might be taken down sometime in the future. Harman also commercially provides a working version of the player if you\'re not on Linux. Please be aware that the only purpose of fg-catalog is to provide means of cataloging flash games, not helping with hacking or obtaining flash games or the flash player itself. So please don\'t ask us to provide any actual flash plugins that work past the expiry date.')
        ]})
    }
};
