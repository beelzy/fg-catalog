var m = require('mithril');
var SVG = require('./helper/SVG');
var Controls = require('./views/Controls');
var nwFunctions = require('./helper/NWFunctions');

require('./remote.scss');

nwFunctions.win.on('close', function() {
    this.close(false);
});

m.render(document.body, [SVG.icon('flash'), m(Controls, {remote: true})]);
