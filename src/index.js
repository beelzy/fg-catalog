var m = require('mithril');
if (!window.webMode) {
    var Settings = require('./models/Settings');
    var nwFunctions = require('./helper/NWFunctions');
}
var Frame = require('./views/Frame');

require('./style.scss');

var routeInfo = function(vnode) {
    return {route: vnode.attrs.route, id: vnode.attrs.id};
};

// Initial app setup
if (!window.webMode) {
    Settings.initFlashTrust();
    nwFunctions.initWindowPolicy();
    nwFunctions.initChromePolicy();
} else {
    var configsLoaded = 0;
    window.addEventListener('dataLoaded', function() {
        configsLoaded++;
        if (configsLoaded === 5) {
            m.redraw();
        }
    });
    window.addEventListener('dataError', function(e) {
        configsLoaded = 6;
        m.redraw();
    });
    if (window.RufflePlayer) {
        window.RufflePlayer.config.allowNetworking = 'internal';
    }
}

m.route(document.body, '/', {
    '/': {
        render: function(vnode) {
            return m(Frame, {loading: configsLoaded < 5, routeInfo: routeInfo(vnode)})
        }
    },
    '/tag/:id': {
        onmatch: function(args) {
            args.route = 'tag';
        },
        render: function(vnode) {
            return m(Frame, {loading: configsLoaded < 5, routeInfo: routeInfo(vnode)})
        }
    },
    '/creator/:id': {
        onmatch: function(args) {
            args.route = 'creator';
        },
        render: function(vnode) {
            return m(Frame, {loading: configsLoaded < 5, routeInfo: routeInfo(vnode)})
        }
    }
});
